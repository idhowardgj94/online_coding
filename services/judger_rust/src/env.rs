#[allow(dead_code)]
mod env_type {
  pub const UNLIMITED: i32 = -1;
  pub const VERSION: i32 = 0x020101;
  pub const RESULT_SUCCESS: i32 = 0;
  pub const RESULT_WRONG_ANSWER: i32 = -1;
  pub const RESULT_CPU_TIME_LIMIT_EXCEEDED: i32 = 1;
  pub const RESULT_REAL_TIME_LIMIT_EXCEEDED: i32 = 2;
  pub const RESULT_MEMORY_LIMIT_EXCEEDED: i32 = 3;
  pub const RESULT_RUNTIME_ERROR: i32 = 4;
  pub const RESULT_SYSTEM_ERROR: i32 = 5;
  pub const ERROR_INVALID_CONFIG: i32 = -1;
  pub const ERROR_FORK_FAILED: i32 = -2;
  pub const ERROR_PTHREAD_FAILED: i32 = -3;
  pub const ERROR_WAIT_FAILED: i32 = -4;
  pub const ERROR_ROOT_REQUIRED: i32 = -5;
  pub const ERROR_LOAD_SECCOMP_FAILED: i32 = -6;
  pub const ERROR_SETRLIMIT_FAILED: i32 = -7;
  pub const ERROR_DUP2_FAILED: i32 = -8;
  pub const ERROR_SETUID_FAILED: i32 = -9;
  pub const ERROR_EXECVE_FAILED: i32 = -10;
  pub const ERROR_SPJ_ERROR: i32 = -11;
}

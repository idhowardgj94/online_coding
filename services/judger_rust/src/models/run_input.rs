use serde::{Serialize, Deserialize};
// TODO args 應該是個 hashMap，如果執行直譯語言，需要使用到（直譯語言的exepath 是指直譯器的位置，其它
// 參數應該透過 args
#[derive(Debug, PartialEq, Serialize, Deserialize, Clone)]
pub struct RunInput {
    pub sandbox: String,
    pub max_cpu_time: i32,
    pub max_real_time: i32,
    pub max_memory: i32,
    pub max_stack: i32,
    pub max_output_size: i32,
    pub max_process_number: i32,
    pub exe_path: String,
    pub input_path: String,
    pub output_path: String,
    pub result_path: String,
    pub error_path: String,
    pub args: Vec<String>,
    pub env: Vec<String>,
    pub log_path: String,
    pub seccomp_rule_name: String,
    pub uid: i32,
    pub gid: i32,
    pub memory_limit_check_only: i32
}

impl RunInput {
    pub fn default() -> Self {
        RunInput {
            sandbox: String::new(),
            max_cpu_time: 1000, 
            max_real_time: 2000, 
            max_memory: 128 * 1024 * 1024, 
            max_stack: 32 * 1024 * 1024, 
            max_output_size:10000, 
            max_process_number: 1, 
            exe_path: "test/main".to_owned(),
            input_path: "test/1.in".to_owned(), 
            output_path: "test/1.out".to_owned(), 
            error_path: "test/1.out".to_owned(),
            result_path: "test/1.result".to_owned(),
            args: Vec::new(),  
            env:  Vec::new(), 
            log_path: "judge.log".to_owned(), 
            seccomp_rule_name: "c_cpp".to_owned(), 
            uid: 0, 
            gid: 0, 
            memory_limit_check_only: 0
        }
    }
}
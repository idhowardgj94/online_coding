// rust 是強型態的語言，不用在function內另外判斷型態
extern crate subprocess;
extern crate serde_json;
use subprocess::{ Popen, PopenConfig, Redirection };
use std::result::Result;
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;
use crate::models::run_input::{RunInput};

pub fn run(
  input: RunInput
) -> Result<(), Box<dyn Error>> {
  let proc_args = prepare_argument(&input);


  // perform sandbox
  let mut p = Popen::create(&proc_args, PopenConfig {
    stdout: Redirection::Pipe,
    stderr: Redirection::Pipe,
    ..Default::default()
  })?;

  // Obtain the output from the standard streams.
  let (out, _) = p.communicate(None)?;

  if let Some(o) = &out {
   println!("{:?}", o); 
  }
  
  if let Some(exit_status) = p.poll() {
      // the process has finished
      println!("{:?}", exit_status);
  } else {
      // it is still running, terminate it
      p.terminate()?;
  }
 
  // TODO env
  let mut file = File::create(&input.result_path)?;
  if let Some(val) = out {
    file.write_all(val.as_bytes()).expect("something when wrong");
  } else {
    panic!("something wrong when write file");
  }
  
  Ok(())
}

fn prepare_argument(input: &RunInput) -> Vec<String>{
   // 組合 args
   let mut proc_args: Vec<String> = Vec::new();
    // sandbox: c system call
    proc_args.push(String::from(&input.sandbox));
   // args and env (list)
    // 原來我已經做好了，棒棒 TODO
    // 以java 來說，/bin/java 是 exe_path，其它的參數都使用 args 帶入，
    // ref: language.py `run.command` in onlineJudge
   for arg in &input.args {
     proc_args.push(format!("--args={}", arg));
   }
   for e in &input.env {
     proc_args.push(format!("--env={}", e));
   }
 
   // intger arg
   proc_args.push(format!("--max_cpu_time={}", input.max_cpu_time));
   proc_args.push(format!("--max_real_time={}", input.max_real_time));
   proc_args.push(format!("--max_memory={}", input.max_memory));
   proc_args.push(format!("--max_stack={}", input.max_stack));
   proc_args.push(format!("--max_output_size={}", input.max_output_size));
   proc_args.push(format!("--max_process_number={}", input.max_process_number));
   proc_args.push(format!("--uid={}", input.uid));
   proc_args.push(format!("--gid={}", input.gid));
   proc_args.push(format!("--memory_limit_check_only={}", input.memory_limit_check_only));
 
   // string arg
   // exe_path： 執行檔的路徑，根據語言不同，可能是程式直譯器
   proc_args.push(format!("--exe_path={}", format!("{}", input.exe_path)));
   proc_args.push(format!("--input_path={}", format!("{}", input.input_path)));
   proc_args.push(format!("--output_path={}", format!("{}", input.output_path)));
   proc_args.push(format!("--error_path={}",format!("{}", input.error_path)));
   proc_args.push(format!("--log_path={}", format!("{}", input.log_path)));
 
   // seccomp_rule_name
   if input.seccomp_rule_name != "" {
     proc_args.push(format!("--seccomp_rule={}", input.seccomp_rule_name));
   }

    println!("{:?}", proc_args);
   proc_args
}
# 注意事項

* use nightly version of rust

```bash 
  rustup override set nighty
  sudo lsof -i -P -n | grep LISTEN    
 ```

 * (can not find derive error)https://github.com/serde-rs/json/issues/564

 ## test with output

 ```bash
 cargo test -- --nocapture
# or
rustc --test main.rs; ./main
# or
cargo watch “test -- --nocapture”
# 這個指令好像不能用了？
```

ref :https://medium.com/@ericdreichert/how-to-print-during-rust-tests-619bdc7ccebc
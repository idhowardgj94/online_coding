#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
pub mod utils;
mod judge;
mod commons;
use std::env;
fn main() {
  let path = env::current_dir().unwrap();
  println!("current directory is {}", path.display());
  let mut rocket = rocket::ignite();
  rocket = judge::mount(rocket);
  rocket.launch();
}

use serde::{Serialize, Deserialize};
// language 跟 compile 有關...
#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Language {
  pub version: String,
  pub  language: String,
  pub  config: LangConfig,
}

#[derive(Debug, PartialEq, Serialize,Deserialize)]
pub struct LangConfig {
  pub compile: Compile,
  pub run: Run
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Compile {
  pub src_name: String,
  pub exe_name: String,
  pub max_cpu_time: i32,
  pub max_real_time: i32,
  pub max_memory: i32,
  pub compile_command: String,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
pub struct Run {
  pub command: String,
  pub seccomp_rule: String,
  pub env: Vec<String>,
}


// c_lang_config = {
//   "compile": {
//       "src_name": "main.c",
//       "exe_name": "main",
//       "max_cpu_time": 3000,
//       "max_real_time": 5000,
//       "max_memory": 128 * 1024 * 1024,
//       "compile_command": "/usr/bin/gcc -DONLINE_JUDGE -O2 -w -fmax-errors=3 -std=c99 {src_path} -lm -o {exe_path}",
//   },
//   "run": {
//       "command": "{exe_path}",
//       "seccomp_rule": "c_cpp",
//       "env": default_env
//   }
// }

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct CompileRequest {
    pub compile_type: String,
    pub code: String
}

#[derive(Serialize, Deserialize)]
pub struct Response {
    pub status: String,
    pub  msg: Option<String>,
    pub output: Option<String>,
    pub result: Option<i32>,
}
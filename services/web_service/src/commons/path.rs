use serde::{Deserialize, Serialize};
use std::fs::File;
use std::result::Result;
use std::error::Error;
use std::io::Read;

#[derive(Serialize, Deserialize, Debug)]
pub struct PathEnv {
  pub exe_path: String,
  pub input_path: String,
  pub sandbox: String,
  pub output_path: String,
  pub error_path: String,
  pub log_path: String,
  pub base_path: String,
  pub test_case: String
}

impl PathEnv {
  pub fn default() -> Result<Self, Box<dyn Error>> {
    let mut file = File::open("web_service/config/path.json")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;
    let r: PathEnv = serde_json::from_str(&contents)?;
    Ok(r)
  }
}

#[cfg(test)]
mod path_test {
  use super::PathEnv;
  #[test]
  fn test_get_default_path() {
    if let Ok(res) =  PathEnv::default() {
      assert_eq!("test", res.exe_path);
    }  else if let Err(e) = PathEnv::default() {
      panic!("{:?}", e);
    }
  }
}
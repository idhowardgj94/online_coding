use crate::commons::path::PathEnv;
use std::path::Path;
use std::env;
use rocket_contrib::json::Json;
use crate::commons::http;

// silent is golden
pub fn set_env() {
    let env_path = PathEnv::default().unwrap();
    let root = Path::new(&env_path.base_path);
    env::set_current_dir(&root).unwrap();
}

pub fn fail_response(msg: String) -> Json<http::Response> {
    Json(http::Response {
        status: String::from("fail"),
        msg: Some(msg),
        result: None,
        output: None,
    })
}
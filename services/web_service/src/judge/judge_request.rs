use serde::{Serialize, Deserialize};
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JudgeRequest {
  pub src: Option<String>,
  pub language: Option<String>,
  pub max_cpu_time: Option<i32>,
  pub max_memory: Option<i32>,
  pub test_case_id: Option<i32>,
  pub output: Option<bool>,
}
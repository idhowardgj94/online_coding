use crate::commons::path::PathEnv;
use judger_rust::models::run_input::RunInput;
use std::process::Command;
use uuid::Uuid;
use crate::judge::judge_request::JudgeRequest;
use std::fs;
use std::fs::File;
use std::io::Write;
use std::error::Error;

// TODO 紀錄
#[derive(Clone, Debug)]
pub struct CompileAndJudge {
    lang: String,
    exe_command: String,
    folder: Box<String>,
    src_file: String,
    input: RunInput,
    uuid: Uuid,
    data: JudgeRequest,
    answer_path: String,
}

impl CompileAndJudge {
    pub fn read_output(&mut self) -> String {
        fs::read_to_string(format!("{}", self.input.output_path)).unwrap()
    }

    pub fn read_answer(&mut self) -> String {
        fs::read_to_string(self.answer_path.clone()).unwrap()
    }

    pub fn result_compare(&mut self) -> i32 {
        println!("output: {}", self.read_output());
        println!("answer: {}", self.read_answer());
        if self.read_output() == self.read_answer() {
            1
        } else {
            -1
        }
    }
    // 產生資料夾，input檔案，output資料夾
    pub fn generate_path(&mut self) -> Result<(), Box<dyn Error>> {
        let src = self.data.src.to_owned().unwrap();
        let folder = format!("exe/{uuid}", uuid = self.uuid);
        self.folder = Box::from(folder.to_owned());
        // TODO chain error
        fs::create_dir_all(&folder).unwrap();
        let mut pos = 0;

        let lang = self.lang.to_owned();
        // 補上其他語言
        let src_file = match lang {
            _ if lang == "c" =>  format!("{}/{}.c", folder, self.uuid),
            _ => format!("{}/{}.c", folder, self.uuid)
        };

        let mut buffer = File::create(src_file.to_owned()).expect("error when create file");

        let byte = src.as_bytes();
        while pos < src.len() {
            let byte_written = buffer.write(&byte[pos..]).expect("error when write src file");
            pos += byte_written;
        }
        // end of src file...
        self.src_file = src_file.to_owned();
        Ok(())
    }

    pub fn compile(&mut self) {
        let env_path = PathEnv::default().unwrap();
        // compile
        // TODO other lang
        Command::new("sh")
            .arg("-c")
            .arg(format!("gcc {} -o {}/{}", self.src_file, *self.folder ,self.uuid))
            .output()
            .expect("failed to execute process");
        // TODO other language
        self.exe_command = format!("{}/{}/{}/{}", env_path.base_path,env_path.exe_path, self.uuid, self.uuid );
    }

    pub fn get_input(&mut self) -> RunInput {
        let env_path = PathEnv::default().unwrap();
        let input = self.data.clone();
        let mut ret = RunInput::default();

        if let Some(v) = input.max_memory {
            ret.max_memory = v;
        };

        if let Some(v) = input.max_cpu_time {
            ret.max_cpu_time = v;
        }
        // error路徑 + 檔名

        ret.error_path = format!("exe/{}/{}.log", self.uuid, self.uuid);
        // TODO recursive path
        let test_case_id = input.test_case_id.unwrap_or_else(|| 1);
        ret.exe_path = self.exe_command.to_owned();
        ret.sandbox = format!("{}/{}", env_path.base_path,  env_path.sandbox);
        // <input>/<uuid>/<test_num>.in | <input>/<test_case_id>/<test_num>.in
        ret.input_path = format!("{}/{}/{}/1.in", env_path.base_path ,env_path.input_path, test_case_id);
        self.answer_path =  format!("{}/{}/{}/1.out", env_path.base_path, env_path.input_path, test_case_id);
        // <output>/<uuid>/<test_num>.out
        ret.output_path = format!("{}/{}/{}/1.out", env_path.base_path, env_path.exe_path, self.uuid);
        ret.result_path = format!("{}/{}/{}/1.json",  env_path.base_path, env_path.exe_path,self.uuid);

        // <err>/<uuid>.err
        ret.error_path = format!("{}/{}/{}/1.err", env_path.base_path, env_path.error_path, self.uuid);
        // <err>/<uuid>.log
        ret.log_path = format!("{}/{}/{}/1.log", env_path.base_path, env_path.error_path, self.uuid);
        self.input = ret.clone();
        println!("{:?}", self.input);

        ret
    }

    pub fn new(data: JudgeRequest) -> Self {
        CompileAndJudge {
            lang: "".to_string(),
            folder: Box::new(String::new()),
            exe_command: "".to_string(),
            input: RunInput::default(),
            uuid: Uuid::new_v4(),
            src_file: "".into(),
            data,
            answer_path: String::new()
        }
    }
}

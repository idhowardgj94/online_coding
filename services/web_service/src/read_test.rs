

#[cfg(test)]
mod test_read_config {
  use strfmt::strfmt;
  use std::collections::HashMap;
  use std::env;
  use std::fs;
  use crate::commons::language::Language;
  #[test]
  fn read_and_deserized() {
    let path = env::current_dir().unwrap();
    println!("The current directory is {}", path.display());
    let contents = fs::read_to_string("./src/config/c_lang_config.yml").unwrap();
   //     .expect("Something went wrong reading the file");
    let deserialized: Language = serde_yaml::from_str(&contents).unwrap();
    assert_eq!("c", deserialized.language);
  }
  #[test]
  fn test_strfmt_work() {
    let mut vars = HashMap::new();
    vars.insert("foo".to_owned(), "howard");
    let fmt = "hello, {foo}";
    assert_eq!(strfmt(&fmt, &vars).unwrap(), "hello, howard");
  }
}

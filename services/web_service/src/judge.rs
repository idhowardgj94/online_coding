use crate::commons::http;
pub mod judge_request;
mod compile_and_judge;

use judge_request::JudgeRequest;
use rocket;
use rocket_contrib::json::Json;
use judger_rust::run;
use crate::utils::{set_env, fail_response};
use crate::judge::compile_and_judge::CompileAndJudge;

pub fn mount(rocket: rocket::Rocket) -> rocket::Rocket {
  rocket.mount("/judge", routes![judge])
}

#[post("/", format="application/json", data="<data>")] 
fn judge(data: Json<JudgeRequest>) -> Json<http::Response> {
    set_env();
    let mut c = CompileAndJudge::new(data.0.to_owned());
    if let Err(_) = c.generate_path() {
        return fail_response("something went wrong when create path".into());
    }
    c.compile();
    // call sandbox to exec
    let result = run::run(c.get_input());
    match result {
        Err(_) => {
            return fail_response("error when execute code".to_owned())
        },
        _ => ()
    }
    // TODO input file mappping to output file
   let contents = c.read_answer();
    // get compile result
    Json(http::Response {
      status: String::from("success"),
      msg: Some(String::from("try to see what happened!")),
      output: Some(contents),
      result: Some(c.result_compare()),
    })
}
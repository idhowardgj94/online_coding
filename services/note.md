## Judger 安裝步驟

1. `apt install seccomp`
1. `cmake CMakeLists.txt`
1. `make`

## git submodule 指令

[手動設定](https://stackoverflow.com/questions/36386667/how-to-make-an-existing-directory-within-a-git-repository-a-git-submodule) 如果已經存在有git的資料夾可以照著此步驟設定

git 指令
---

ref from [here](https://blog.wu-boy.com/2011/09/introduction-to-git-submodule/)

```bash
git submodule add <repository> [<path>]</path></repository>
```

## run test wih println!

```bash
cargo test -- --nocapture
```
const { createProxyMiddleware } = require('http-proxy-middleware');

module.exports = exports = function(app) {
  app.use(
    '/judge',
    createProxyMiddleware({
      target: 'http://localhost:8002',
      changeOrigin: true,
    })
  );
};
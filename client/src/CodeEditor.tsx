import React from 'react';
import Editor from 'react-simple-code-editor';
import Button from 'react-bootstrap/Button';
import Prism, { highlight, languages } from 'prismjs';
import 'prismjs/themes/prism.css'

const code = `
#include<stdio.h>;

int main(void) {
  printf("hello, world!");
  return 0;
}
`;
 
export class CodeEditor extends React.Component<any, any> {
  constructor(props: any) {
    super(props)
    this.state =  { 
      code,
      showResult: false,
      result: 0,
      output: ''
     };
  }

  onClickHandler() {
    console.log('inside onClickHandler', this.state.code)
    const { code } = this.state
    console.log(code)
    fetch("/judge", {
      method: "POST", 
      headers: {
        'Content-type': 'application/json',

      },
      body: JSON.stringify({
        src: code
      })
    }).then(d => d.json())
    .then(data => {
      console.log('data', data);
      this.setState({ result: data['result'], output: data['output'], showResult: true})
    })
  }
  render() {
    const { showResult, output, result } = this.state
    return (
      <>
      <Editor
        value={this.state.code}
        onValueChange={(code) => this.setState({ code })}
        highlight={(code) => highlight(code, languages.clike, 'c')}
        padding={10}
        style={{
          position: 'relative',
          minWidth: '40vw',
          backgroundColor: 'rgba(0, 0, 0, 0.05)',
          fontFamily: '"Fira code", "Fira Mono", monospace',
          fontSize: 14,
        }}
      />
      
      <Button variant="primary" className="mt-4" onClick={() => this.onClickHandler()}>送出</Button>
      { showResult ? (
        <p className="mt-2">
        <ul>
  <li>輸出： {output}</li>
          <li>結果：{ result=== 0 ? '對了' : '錯！'}</li>
        </ul>
      </p>
      ) : null} 
      
      </>
    );
  }
}
import React from 'react';
import './App.scss';
import { CodeEditor } from './CodeEditor';
import styled from 'styled-components';
const ReactMarkdown = require('react-markdown')

const Header = styled.h1`
text-align: center;
`

const Container = styled.div`
display: grid;
width: 80%;
grid-gap: 20px;
margin: 0 auto;
grid-template-columns: 50% 50%;
grid-template-areas: "content code";
`

const Content = styled.div`
margin: 10px;
grid-area: content;
`

const Code = styled.div`
grid-area: code;
`
const markdown = `
## 題目： 1480. hello, world

* 難易度： 易
*  題目：請印出 “hello, Howard, you are handsome“ 到螢幕上。

`
function App() {
  return (
    <div>
      <Header>C coding online 服務</Header>
      <Container>
        <Content>
          <ReactMarkdown source={markdown} />
        </Content>

        <Code>
          <h2>請在下面作答，成功後點送出</h2>
          <CodeEditor />
        </Code>
      </Container>

    </div>
  );
}

export default App;
